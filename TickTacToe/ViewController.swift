//
//  ViewController.swift
//  TickTacToe
//
//  Created by Juan Pastor Trigueros on 21/6/15.
//  Copyright (c) 2015 Juan Pastor Trigueros. All rights reserved.
//

import UIKit
import GoogleMobileAds

struct GameStatus {
    
    var lastPlay = 0
    var winner = 0
    var restartCounter = 0
}

class ViewController: UIViewController {

    @IBOutlet weak var bannerView: GADBannerView!
    
    @IBOutlet weak var button0: UIButton!
    @IBOutlet weak var button1: UIButton!
    @IBOutlet weak var button2: UIButton!
    @IBOutlet weak var button3: UIButton!
    @IBOutlet weak var button4: UIButton!
    @IBOutlet weak var button5: UIButton!
    @IBOutlet weak var button6: UIButton!
    @IBOutlet weak var button7: UIButton!
    @IBOutlet weak var button8: UIButton!
    
    //@IBOutlet weak var myLabel: UILabel!
    //@IBOutlet weak var logLabel: UILabel!
    @IBOutlet weak var waitView: UIView!
    
    var interstitial: GADInterstitial!
    
    var gameStatus = GameStatus()
    var gameState = [0, 0, 0, 0, 0, 0, 0, 0, 0]
    var winningCombinations = [[0,1,2], [3,4,5], [6,7,8], [0,3,6], [1,4,7], [2,5,8], [0,4,8], [2,4,6]]
    var preferedAIMoves = [4, 0, 2, 6, 8, 1, 3, 7, 5]
    var adUnit = "ca-app-pub-8583050969295844/4647061731"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.interstitial = self.createAndLoadAdIntersitial()
        
        self.bannerView.adUnitID = self.adUnit
        
        self.bannerView.rootViewController = self
        self.view.addSubview(bannerView)
        
        self.bannerView.loadRequest(GADRequest())
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    @IBAction func buttonPressed(sender: UIButton)
    {
        var tagpos = sender.tag - 100
        if gameState[tagpos] == 0 && gameStatus.winner == 0
        {
            var image = UIImage()
            gameState[tagpos]   = 1
            gameStatus.lastPlay = 1
                
            drawBoard()
            
            let time = dispatch_time(DISPATCH_TIME_NOW, Int64(0.5 * Double(NSEC_PER_SEC)))
            dispatch_after(time, dispatch_get_main_queue(), {
                self.IAMovement()
            })
            
            checkWinner()
            checkTie()
            
        }
        
    }
    
    func IAMovement()
    {
        if gameStatus.winner == 0 && contains(gameState, 0) {
         
            gameStatus.lastPlay = -1
            bestMove()
            drawBoard()
            checkWinner()
            checkTie()
        }
    }
    
    func bestMove()
    {
        var maxScore = -9999
        var score = 0
        var posSelected = 0
        for cell in preferedAIMoves
        {
            if gameState[cell] == 0
            {
                gameState[cell] = -1
                score = self.min(gameState)
                if score > maxScore
                {
                    maxScore = score
                    posSelected = cell
                }
                gameState[cell] = 0
            }
        }
        gameState[posSelected] = -1
    }
    
    func min(game: [Int]) -> Int
    {
        var testgame = game
        var aux = 0
        var best = 9999
        
        if checkPossibleWinner(testgame) == -1 {
            return 1
        }
        if !contains(testgame, 0) {
            return 0
        }
        
        for (var i = 0; i < testgame.count; i++)
        {
            if testgame[i] == 0 {
                testgame[i] = 1
                aux = self.max(testgame)
                if aux < best {
                    best = aux
                }
                testgame[i] = 0
            }
        }
        return best;
    }
    
    func max(game: [Int]) -> Int
    {
        var testgame = game
        var aux = 0
        var best = -9999;
        
        if checkPossibleWinner(testgame) == 1 {
            return -1
        }
        
        if !contains(testgame, 0) {
            return 0
        }
        
        for (var i = 0;i < testgame.count;i++)
        {
            if testgame[i] == 0 {
                testgame[i] = -1
                aux = self.min(testgame)
                if aux > best {
                    best = aux
                }
                testgame[i] = 0
            }
        }
        return best;
    }
    
    func checkPossibleWinner(game: [Int]) -> Int
    {
        var virtualWinner = 0;
        for combination in winningCombinations
        {
            if game[combination[0]] == game[combination[1]] &&
                game[combination[0]] == game[combination[2]] &&
                game[combination[0]] != 0
            {
                virtualWinner = game[combination[0]]
            }
        }
        return virtualWinner
    }
    
    func checkWinner()
    {
        gameStatus.winner = self.checkPossibleWinner(gameState)
        
        if gameStatus.winner != 0 {
            if gameStatus.winner == 1 {
                
            }
            else {
                let userDefaults = NSUserDefaults.standardUserDefaults()
                let loses = userDefaults.integerForKey("loses")
                let ties  = userDefaults.integerForKey("ties")
                
                if loses > 0 {
                    userDefaults.setInteger(loses + 1, forKey: "loses")
                }
                else {
                    let loses = 1
                    userDefaults.setInteger(loses, forKey: "loses")
                }
                
                userDefaults.synchronize() // don't forget this!!!!
                
                JSSAlertView().show(
                    self, // the parent view controller of the alert
                    title: "X Wins", // the alert's title
                    text: "Loses: \(loses + 1) Ties: \(ties) \n Click restart for new game"
                )
            }

        }
    }
    
    func checkTie()
    {
        if  !contains(gameState, 0) && gameStatus.winner == 0
        {
            let userDefaults = NSUserDefaults.standardUserDefaults()
            let ties  = userDefaults.integerForKey("ties")
            let loses = userDefaults.integerForKey("loses")
            
            if ties > 0 {
                userDefaults.setInteger(ties + 1, forKey: "ties")
            }
            else {
                let ties = 1
                userDefaults.setInteger(ties, forKey: "ties")
            }
            
            JSSAlertView().show(
                self, // the parent view controller of the alert
                title: "Tie game", // the alert's title
                text: "Loses: \(loses) Ties: \(ties + 1) \n Click restart for new game"
            )
        }
    }
    
    func drawBoard()
    {
        var index = 0
        var btnAutoSelect : UIButton
        var circleImg : UIImage = UIImage(named: "o.png")!
        var xImg : UIImage = UIImage(named: "x.png")!
        for cell in gameState
        {
            btnAutoSelect = (view.viewWithTag(100 + index) as? UIButton)!
            btnAutoSelect.setImage(nil, forState: UIControlState.Normal)
            if cell == 1
            {
                btnAutoSelect.setImage(circleImg, forState: UIControlState.Normal)
            }
            else if cell == -1
            {
                btnAutoSelect.setImage(xImg, forState: UIControlState.Normal)
            }
            index++
        }

    }
    
    @IBAction func restartPressed(sender: UIButton)
    {
        gameStatus.restartCounter++
        
        if self.interstitial.isReady && gameStatus.restartCounter % 3 == 0        {
            self.interstitial.presentFromRootViewController(self)
            self.interstitial = self.createAndLoadAdIntersitial()
        }
        
        gameStatus.winner = 0
        gameState = [0, 0, 0, 0, 0, 0, 0, 0, 0]
        
        if gameStatus.lastPlay == 1
        {
            gameState = [0, 0, 0, 0, -1, 0, 0, 0, 0]
            button4.setImage(UIImage(named: "x.png")!, forState: UIControlState.Normal)
        }
        
        drawBoard()
    }
    
    func createAndLoadAdIntersitial() -> GADInterstitial
    {
        var ad = GADInterstitial(adUnitID: self.adUnit)
        var intersitialRequest = GADRequest()
        
        intersitialRequest.testDevices = [ kDFPSimulatorID ];
        ad.loadRequest(intersitialRequest)
        
        return ad
    }
}
